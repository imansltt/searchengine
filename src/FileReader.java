import java.io.FileInputStream;
import java.io.IOException;

public class FileReader {
    private static FileReader fileReader = null;

    public static FileReader getFileReader(){
        if (fileReader == null){
            fileReader = new FileReader();
        }
        return fileReader;
    }

    public String [] getWords(String url) throws IOException {
        String fileText = getString(url);
        fileText.replace("\n", " ");
        String [] words = fileText.split(" ");
        return words;
    }

    public String getString(String url) throws IOException {
        String string = "";
        FileInputStream fileInputStream = new FileInputStream(url);
        byte [] allBytes = fileInputStream.readAllBytes();
        for (byte temp : allBytes){
            string += (char)temp;
        }
        string.toLowerCase();
        return string;
    }
}
